This is libinotify - a developer-friendly interface to using the inotify
file monitoring system provided by recent versions of the Linux kernel.

libinotify offers a very simple callback-based interface and automatic glib
mainloop integration.  For most users, there are only 2 interesting functions:

inotify_monitor_add -- register a new watch

  Allows you to register a new watch on a specific file.  You may specify
  the filename, the native inotify event mask and a callback and user_data
  pointer.  This function returns an INotifyHandle pointer.

inotify_monitor_remove -- unregister an existing watch

  Given an INotifyHandle pointer, this function will unregister the watch.


In addition to taking care of the low-level watch registering and event
processing, libinotify acts as a multiplexer and will handle multiple watches
on a single inode in the way that the developer probably expects.

Any applications that use libinotify should be aware that it is not yet API
or ABI stable.  It is very likely that the interfaces will undergo some amount
of change in the future.

libinotify is made available under the terms of version 2.1 of the LGPL.

All bug reports and feature requests should be made through the GNOME bug
tracking system at http://bugzilla.gnome.org/.

If you have any other comments or questions, they should be addressed to
Ryan Lortie <desrt@desrt.ca>.

