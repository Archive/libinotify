#include <stdio.h>
#include <stdlib.h>

#include <linux/inotify.h>

#include <glib.h>

#include <libinotify/libinotify.h>


gchar *
get_events (guint32 event_type)
{
	GString *s;

	s = g_string_new ("");

	if (event_type & IN_ACCESS) {
		s = g_string_append (s, "IN_ACCESS | ");
	}	
	if (event_type & IN_MODIFY) {
		s = g_string_append (s, "IN_MODIFY | ");
	}	
	if (event_type & IN_ATTRIB) {
		s = g_string_append (s, "IN_ATTRIB | ");
	}	
	if (event_type & IN_CLOSE_WRITE) {
		s = g_string_append (s, "IN_CLOSE_WRITE | ");
	}	
	if (event_type & IN_CLOSE_NOWRITE) {
		s = g_string_append (s, "IN_CLOSE_NOWRITE | ");
	}	
	if (event_type & IN_OPEN) {
		s = g_string_append (s, "IN_OPEN | ");
	}	
	if (event_type & IN_MOVED_FROM) {
		s = g_string_append (s, "IN_MOVED_FROM | ");
	}	
	if (event_type & IN_MOVED_TO) {
		s = g_string_append (s, "IN_MOVED_TO | ");
	}	
	if (event_type & IN_CREATE) {
		s = g_string_append (s, "IN_CREATE | ");
	}	
	if (event_type & IN_DELETE) {
		s = g_string_append (s, "IN_DELETE | ");
	}	
	if (event_type & IN_DELETE_SELF) {
		s = g_string_append (s, "IN_DELETE_SELF | ");
	}	
	if (event_type & IN_MOVE_SELF) {
		s = g_string_append (s, "IN_MOVE_SELF | ");
	}	
	if (event_type & IN_UNMOUNT) {
		s = g_string_append (s, "IN_UNMOUNT | ");
	}	
	if (event_type & IN_Q_OVERFLOW) {
		s = g_string_append (s, "IN_Q_OVERFLOW | ");
	}	
	if (event_type & IN_IGNORED) {
		s = g_string_append (s, "IN_IGNORED | ");
	}	
	
	/* helper events */
	if (event_type & IN_CLOSE) {
		s = g_string_append (s, "IN_CLOSE* | ");
	}	
	if (event_type & IN_MOVE) {
		s = g_string_append (s, "IN_MOVE* | ");
	}
	
	/* special flags */	
	if (event_type & IN_MASK_ADD) {
		s = g_string_append (s, "IN_MASK_ADD^ | ");
	}	
	if (event_type & IN_ISDIR) {
		s = g_string_append (s, "IN_ISDIR^ | ");
	}	
	if (event_type & IN_ONESHOT) {
		s = g_string_append (s, "IN_ONESHOT^ | ");
	}	

	s->len -= 3;

	return g_string_free (s, FALSE);	
}


void 
callback (INotifyHandle *handle,
	  const char    *monitor_name,
	  const char    *filename,
	  guint32        event_type,
	  guint32        cookie,
	  gpointer       user_data)
{
	gchar *event_type_str = get_events (event_type);
	
	g_print ("monitor:'%s', file:'%s', event_type:%d -> %s\n",
		monitor_name, filename ? filename : "", 
		event_type, event_type_str, cookie);

	g_free (event_type_str);
}

int 
main (int argc, char *argv[])
{
	GMainLoop      *main_loop;
	INotifyHandle **handle;
	guint32	        mask = IN_ALL_EVENTS;
	int             watches = 0;
	unsigned long   flags = 0;
	int             i;
		
	if (argc < 2) {
		fprintf (stderr, "usage: %s [=]<file>|<dir> ...\n", argv[0]);
		return EXIT_FAILURE;
	}

	handle = g_new (INotifyHandle *, argc);

	if (! inotify_is_available ()) {
		fprintf (stderr, "libinotify is not available\n");
		return EXIT_FAILURE;
	}

	for (i = 1; i < argc; i++) {
		/* if given filename starts with = use file based mode */
		if (argv[i][0] == '=')
		{
			flags |= IN_FLAG_FILE_BASED;
			argv[i]++;
		}
		else
			flags &= ~IN_FLAG_FILE_BASED;

		g_print ("monitor: adding... on uri:'%s'\n", argv[i]);
		handle[i] = inotify_monitor_add (argv[i], mask, flags,
						 callback, NULL);
		if (handle[i] == NULL)
			fprintf (stderr, "failed to watch '%s'\n", argv[i]);
		else
			watches++;
	}

	if (watches == 0) {
		fprintf (stderr, "failed to register any watches\n");
		return EXIT_FAILURE;
	}

	g_print ("Key:\n"
		 "* = convenience events\n"
		 "^ = special events\n");
	
	main_loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (main_loop);

	for (i = 1; i < argc; i++)
	{
		if (handle[i] != NULL)
		{
			g_print ("monitor: removing... on uri:'%s'\n", argv[i]);
			inotify_monitor_remove (handle[i]);
		}
	}
	
	return EXIT_SUCCESS;
}
