#!/bin/sh

set -e

echo Running aclocal...
aclocal

echo Running autoheader...
autoheader

echo Running libtoolize --force...
libtoolize --force

echo Running automake --add-missing...
automake --add-missing

echo Running autoconf...
autoconf

echo Running ./configure --enable-maintainer-mode "$@"
./configure --enable-maintainer-mode "$@"
